module ChatsideHelper

  def latest_message(group)
    group.messages.last
  end

  def latest_body(group)
    latest_message(group).body
  end

  def latest_image(group)
    latest_message(group).image
  end

end