$(document).on('turbolinks:load', function() {

  function buildHTML(user) {
    var html = `<div class="chat-group-user clearfix">
                  <p class="chat-group-user__name">
                    ${ user.name }
                  </p>
                  <a class="chat-group-user__btn chat-group-user__btn--add" data-user-id="${ user.id }" data-user-name="${ user.name }">
                    追加
                  </a>
                </div>`;
    return html;
  }

  function removeHTML(user) {
    var html = `<div class="chat-group-user clearfix">
                  <p class="chat-group-user__name">
                    ${ user.userName }
                  </p>
                  <a class="chat-group-user__btn chat-group-user__btn--remove">
                    削除
                  </a>
                  <input type="hidden" name="group[user_ids][]" value="${ user.userId }">
                </div>`;
    return html;
  }

  $('#user-search-field').on('keyup', function(e) {
    $('#user-search-result').empty();
    e.preventDefault();

    var name = $('#user-search-field').val();

    $.ajax({
      type: 'GET',
      url: '/users',
      data: { keyword: name },
      dataType: 'json'
    })
    .done(function(data) {
      var html = '';
      if(name.length != 0) {
        $.each(data.users, function(i, user){
          html += buildHTML(user);
        });
      }
      $('#user-search-result').append(html);
    })
    .fail(function() {
      alert("ユーザ検索に失敗しました");
    });
    return false;
  });

  $('#user-search-result').on('click', '.chat-group-user__btn--add', function(user) {
    var user = $(this).data();
    console.log(user);
    //上のuserが{ id, name }の属性名でなく、なぜか{ userId, userName }であるためremoveHTMLを上のようにしている
    var rvmhtml = removeHTML(user);
    $('#chat-group-users').append(rvmhtml);
    $(this).parent().remove();
  });

  $('#chat-group-users').on('click', '.chat-group-user__btn--remove', function() {
    $(this).parent().remove();
  });





});