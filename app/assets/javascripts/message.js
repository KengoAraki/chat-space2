$(document).on('turbolinks:load', function() {

  function buildHTML(message) {
    var image_html = '';
    if(message.image) {
      image_html = `<img src= ${ message.image } alt= ${ message.image }>`;
    }

    var html = `<p class="chat-main__message-name">
                  ${ message.name }
                </p>
                <p class="chat-main__message-time">
                  ${ message.time }
                </p>
                <p class="chat-main__message-body">
                  ${ message.body }
                </p>
                <p class="chat-main__message-image">
                  ${ image_html }
                </p>`;
    return html;
  }

  function scroll() {
    $('.chat-main__body').animate({scrollTop: $('.chat-main__body')[0].scrollHeight}, '500', 'swing');
  }

  function flashNotice(notice) {
    var flash = `<div class="flash-alert-notice">
                  ${ notice }
                </div>`;
    return flash;
  }

  function flashAlert(alert) {
    var flash = `<div class="flash-alert-alert">
                  ${ alert }
                </div>`;
    return flash;
  }

  function autoUpdate() {
    if($('#new_message').val() == 0) {
      $.ajax({
        type: 'GET',
        url: './messages',
        dataType: 'json'
      })
      .done(function(data) {
        $('.chat-main__message.clearfix#319').empty();
        var html = '';
        $.each(data.messages, function(i, message) {
          html += buildHTML(data.messages[i]);
        });
        $('.chat-main__message.clearfix#319').append(html);
        scroll();
      })
      .fail(function() {
        console.log("更新に失敗しました");
      });
    }
  }

  if(document.location.href.match('./messages')) {
    setInterval(autoUpdate, 5000);
  }

  $('#new_message').on('submit', function(e) {
    e.preventDefault();
    var message = new FormData($('#new_message')[0]);
    $('.flash-alert-notice').remove();
    $('.flash-alert-alert').remove();

    $.ajax({
      type: 'POST',
      url: './messages',
      data: message,
      dataType: 'json',
      processData: false,
      contentType: false
    })
    .done(function(message) {
      console.log("メッセージ送信のdata");
      console.log(message);

      var html = buildHTML(message);
      var notice = "メッセージが投稿されました";
      var flash = flashNotice(notice);
      $('body').prepend(flash);
      $('.chat-main__message.clearfix#319').append(html);
      $('#new_message')[0].reset();
      scroll();
    })
    .fail(function() {
      var alert = "投稿できませんでした";
      var falsh = flashAlert(alert);
      $('body').prepend(flash);
    });
    return false;
  });

});