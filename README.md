# README

## users table

|    column    |    type    |             options                  |
|:------------:|:----------:|:------------------------------------:|
|     name     |   string   |                                      |


### Association

  ・has_many :groups, through :members  
  ・has_many :messages  
  ・has_many :members

## messages table
|    column    |    type    |             options                  |
|:------------:|:----------:|:------------------------------------:|
|     body     |    text    |                                      |
|     image    |    text    |                                      |
|     user     | references |         foreign_key:true             |
|     group    | references |         foreign_key:true             |

### Association

  ・belongs_to :user  
  ・belongs_to :group

## groups table
|    column    |    type    |             options                  |
|:------------:|:----------:|:------------------------------------:|
|     name     |   string   |             null:false               |

### Association

  ・has_many :users, through :members  
  ・has_many :messages  
  ・has_many :members

## members table
|    column    |    type    |             options                  |
|:------------:|:----------:|:------------------------------------:|
|     user     | references |          foreign_key:true            |
|     group    | references |          foreign_key:true            |

### Association

  ・belongs_to :user  
  ・belongs_to :group
